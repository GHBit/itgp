﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

    public float speed;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        //Movement Script. Works by getting an input axis and multiplying by speed for movement along both the X and Y axis.
        //deltaTime apparently makes the method independant of FPS.
        //https://docs.unity3d.com/ScriptReference/Input.GetAxis.html used as reference material for GetAxis and deltaTime.
        //De-Commenting float directionY = 1 * speed; and commenting the line below it makes forward movement automatic
        float directionX = Input.GetAxis("Horizontal") * speed;
        //float directionY = 1 * speed;
        float directionY = Input.GetAxis("Vertical") * speed; 
        directionX *= Time.deltaTime; 
        directionY *= Time.deltaTime;
        transform.Translate(directionX, directionY, 0); //0 could theoretically be swapped for a directionZ
	}
}
